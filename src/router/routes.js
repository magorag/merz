import Dashboard from '../pages/Dashboard.vue'
import Cursos from '../pages/Cursos.vue'
import Curso from '../pages/Curso.vue'
import Conteudo from '../pages/Conteudo.vue'
import Usuario from '../pages/Usuario.vue'

export const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard
  },
  {
    path: '/cursos',
    name: 'cursos',
    component: Cursos
  },
  {
    path: '/usuario/:user',
    name: 'usuario',
    component: Usuario
  },
  {
    path: '/:curso',
    name: 'curso',
    component: Curso
  },
  {
    path: '/:curso/:conteudo',
    name: 'conteudo',
    component: Conteudo
  }
]
